This project houses all the source files for JabberAdda; a 
group chat program developed by BhelPuriTrio

Instructions :
##############

  0. Install the following packages :

        - python3-pil
        - python3-pil.imagetk
        - python3-enums
        - python3-tk

     On Ubuntu, these can be installed by running the following commands :

      # sudo apt-get install python3-pil
      # sudo apt-get install python3-pil.imagetk
      # sudo apt-get install python3-tk
      # sudo apt-get install python3-enums

  1. Before executing the program, run the following command :

	# source env.sh
  
  2. To execute the program, run the following command :
	
	# python3.5 main.py <command-line arguments if any>

