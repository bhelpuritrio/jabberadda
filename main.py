from threading import Thread

# Standard Python modules :
import socket
import cmd

# Program specific modules :

# Import manager
import manager

# Import thread functions
import cmd_parser
import packet_processor
import membership_broadcaster

multicast_group_address = "224.56.78.89"
multicast_group_port = 600
unicast_udp_port = 887

# Get the IP address of the local host 
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
local_ip_address = s.getsockname()[0]
s.close()

print ("main >> Instantiating a manager instance")
manager_object = manager.manager(multicast_group_address,multicast_group_port,local_ip_address,unicast_udp_port)
cmdparser_object = cmd_parser.CmdParser()
cmdparser_object.set_manager(manager_object)

print ("main >> Spawning off threads")
packet_processor_thread = Thread(target = packet_processor.packet_receive_thread, args = (1,manager_object))
ucast_processing_thread = Thread(target = packet_processor.ucast_processing_thread, args = (1,manager_object))
mcast_processing_thread = Thread(target = packet_processor.mcast_processing_thread, args = (1,manager_object))
membership_broadcast_thread = Thread(target = membership_broadcaster.membership_broadcast_thread, args = (1,manager_object))
packet_processor_thread.start()
ucast_processing_thread.start()
mcast_processing_thread.start()
membership_broadcast_thread.start()
cmdparser_object.cmdloop()
print ("Gracefully exiting..")
