##########################################################################

# ==========
# manager.py
# ==========

# This module implements an intermediate manager API layer between the
# threads (which exercise the core module functionality) and the core modules.
# This was done to ensure encapsulation of functionalities into modules (following
# the 'separation of concern' principle) as well as implement mutex locking
# functionality to avoid race conditions while accessing common resources;
# such as the cache.

##########################################################################

from threading import Thread
import cache
import threading
import packetsr
import queue
from enums import *
import struct
import getpass
from packstruct import MessageType, PacketHeader
import datetime
import random
import time
from tkinter import *
from ui import *

class manager:
    
    def __init__(self, multicast_group, multicast_port, local_address, local_port ):
        self.counter = 0
        self.quit = 0

        # Cache instance :
        self.cache = cache.cache(local_address,getpass.getuser())
        self.cache_lock = threading.RLock()

        # Unicast & Multicast packet receive queues :
        self.ucast_queue_lock = threading.RLock()
        self.mcast_queue_lock = threading.RLock()
        self.mcast_queue = queue.Queue()
        self.ucast_queue = queue.Queue()

        # Initializing the packetsr module for basic send / receive of 
        # Unicast / Multicast packets across the network
        self.packetsr = packetsr.packetsr(multicast_group, multicast_port, local_address, local_port)

        # "ACK" packet dictionary for use by the state machine which implements 
        # reliable transfer of unicast messages 
        self.ack_dict = dict()
        self.ack_dict_lock = threading.RLock()

        # Start the UI
        self.ui_thread = Thread(target = self.manager_start_ui, args = ([]))
        self.ui_thread.start()
        self.ui_lock = threading.RLock()
        #self.manager_start_ui()
        print ("manager >> Manager initialized !!")

    def manager_check_quit(self):
        return (self.quit == 1)

    def manager_app_quit(self):
        self.quit = 1 
        self.root.quit()

    def debug_msg(message):
        print("manager >> ",message)

    def update_counter(self):
        self.counter+=1
        print("manager >> Counter value is ", self.counter)

       
    #################################
    # Packet S/R related functions :
    #################################

    def manager_wait_for_packet_receive_events(self):
        events = self.packetsr.wait_for_packet_events() 
        return events

    def manager_queue_mcast_packet(self,info):
        self.mcast_queue_lock.acquire()
        self.mcast_queue.put(info)
        self.mcast_queue_lock.release()

    def manager_dequeue_mcast_packet(self):
        self.mcast_queue_lock.acquire()
        if self.mcast_queue.empty():
            info = []
        else:
            info = self.mcast_queue.get()
        self.mcast_queue_lock.release()
        return info

    def manager_queue_ucast_packet(self,info):
        self.ucast_queue_lock.acquire()
        self.ucast_queue.put(info)
        self.ucast_queue_lock.release()

    def manager_dequeue_ucast_packet(self):
        self.ucast_queue_lock.acquire()
        if self.ucast_queue.empty():
            info = []
        else:
            info = self.ucast_queue.get()
        self.ucast_queue_lock.release()
        return info

    def manager_recv_multicast_packet(self):
        return self.packetsr.recv_multicast_packet()

    def manager_recv_unicast_packet(self):
        return self.packetsr.recv_unicast_packet()

    def manager_send_unicast_packet(self,packet,local_address):
        return self.packetsr.send_packet(0,packet,local_address)

    def manager_send_multicast_packet(self,packet):
        return self.packetsr.send_packet(1,packet)

    #################################
    # Jabber file snd/rcv functions :
    #################################

    def manager_tcp_wait_for_connect_requests(self):
        return self.packetsr.tcp_wait_for_connect_requests()

    def manager_tcp_receive_file_async(self):
        async_file_receive_thread = Thread(target = self.manager_tcp_receive_file_from_client, args = ()) 
        async_file_receive_thread.start()

    def manager_tcp_receive_file_from_client(self):
        ret = self.packetsr.tcp_receive_file_from_client()
        senderip = ret[0][0]
        groupid = ret[1]
        filename = ret[2]
        self.manager_handle_message_display("Sent a file - "+filename,groupid,(senderip,800))

    def manager_send_file_async(self,filename,groupid):
        async_file_send_thread = Thread(target = self.manager_send_file, args = (filename,groupid))
        async_file_send_thread.start()

    def manager_send_file(self,filename,groupid):
        memberlist = self.manager_get_memberlist(groupid)
        self_details = self.manager_get_self_details()
        self_ip = self_details[0]
        for member in memberlist:
            if (member != self_ip): # Don't send the file to yourself, duh..!
                self.packetsr.tcp_send_file(filename,member,groupid)

    #####################################
    # Jabber message snd/rcv functions :
    #####################################

    def manager_multicast_membership_info(self,packet):
        obj = PacketHeader()
        obj.SetGroupID("")
        obj.SetMessageType(MessageType.membership_info)
        obj.SetPktSize(len(packet))
        msgid = random.randint(1,9999)
        obj.SetMsgID(msgid)
        obj.SetAckID(0)
        obj.SetTimeStamp(datetime.datetime.now())
        obj.SetReqRes(0)
        obj.SetSeqID(msgid)
        pktheader = obj.FormStringHeader()
        packet = pktheader+packet
        return self.manager_send_multicast_packet(packet)
        
    def manager_send_message(self,message,groupid):
        obj = PacketHeader()
        obj.SetGroupID(groupid)
        obj.SetMessageType(MessageType.send_msg)
        obj.SetPktSize(len(message))
        msgid = random.randint(1,9999)
        obj.SetMsgID(msgid)
        obj.SetAckID(0)
        obj.SetTimeStamp(datetime.datetime.now())
        obj.SetReqRes(0)
        obj.SetSeqID(msgid)
        pktheader = obj.FormStringHeader()
        message = pktheader+message
        memberlist = self.manager_get_memberlist(groupid)
        for member in memberlist:
            async_unicast_message_thread = Thread(target = self.manager_reliable_message_unicast, args = (member,message,msgid+1))
            async_unicast_message_thread.start()
        return SUCCESS

    def manager_reliable_message_unicast(self,dest_ip,packet,ackid):
        print ("manager_reliable_message_unicast() : Sending message asynchronously, ACKID expected is ",ackid, "from ", dest_ip)
        timeout_interval = 0.125
        retry_times = 3
        while retry_times > 0:
            self.manager_send_unicast_packet(packet,dest_ip) 
            time.sleep(timeout_interval)
            ack = self.manager_retrieve_ack((dest_ip,ackid))
            if (ack != ""):
                print("ACK# ",ackid," successfully received from ", dest_ip)
                break
            else:
                print("ACK# ",ackid," not received from ", dest_ip,"; will try ReTX! Tries left :", retry_times)
            retry_times -= 1
        return SUCCESS

    def manager_handle_message_display(self,packet,groupid,src_addr):
        self_membership_list = self.manager_get_self_membership_list()
        membername = self.manager_get_membername(src_addr[0])
        if groupid in self_membership_list:
            text_to_display = membername+"("+str(src_addr[0])+")"+" says : "+str(packet)+"\n"
            self.manager_display_text(groupid,text_to_display)

    #############################
    # ACK Dictionary functions :
    #############################
    
    def manager_store_ack(self,src_ip,ackid,packet):
        self.ack_dict_lock.acquire()
        self.ack_dict[(src_ip,ackid)] = packet
        self.ack_dict_lock.release()

    def manager_retrieve_ack(self,ip_ackid_tuple):
        self.ack_dict_lock.acquire()
        if ip_ackid_tuple not in self.ack_dict.keys():
            ret = ""
        else:
            ret = self.ack_dict[ip_ackid_tuple]
            self.ack_dict.pop(ip_ackid_tuple)
        self.ack_dict_lock.release()
        return ret

    #####################
    # Cache functions :
    #####################

    def manager_search_group(self,groupid):
        self.cache_lock.acquire()
        ret = self.cache.cache_search_group(groupid)
        self.cache_lock.release()
        return ret

    def manager_get_membername(self,memberid):
        self.cache_lock.acquire()
        memberdetails = self.cache.cache_get_memberdetails(memberid) 
        print (memberdetails)
        self.cache_lock.release()
        if (memberdetails):
            return memberdetails['name']
        else:
            return ""

    def manager_get_memberlist(self,groupid):
        if groupid == "":
            return []
        else:
            self.cache_lock.acquire()
            memberlist = self.cache.cache_get_group_members(groupid)  
            self.cache_lock.release()
            return memberlist

    def manager_add_member_details(self,memberid,membername):
        stripped_memberid = memberid[0:15]
        stripped_membername = membername[0:8]
        self.cache_lock.acquire()
        self.cache.cache_add_member(stripped_memberid,stripped_membername) 
        self.cache_lock.release()

    def manager_print_all_member_details(self):
        self.cache_lock.acquire()
        self.cache.cache_print_all_member_details() 
        self.cache_lock.release()

    def manager_print_all_group_details(self):
        self.cache_lock.acquire()
        self.cache.cache_print_all_group_details()
        self.cache_lock.release()

    def manager_add_self_to_group(self,groupid):
        self.cache_lock.acquire()
        ret = self.cache.cache_add_self_to_group(groupid)
        self.cache_lock.release()
        return ret

    def manager_remove_self_from_group(self,groupid):
        self.cache_lock.acquire()
        ret = self.cache.cache_remove_self_from_group(groupid)
        self.cache_lock.release()
        return ret

    def manager_add_group(self,groupid):
        stripped_groupid = groupid[0:19]
        self.cache_lock.acquire()
        ret = self.cache.cache_add_group(stripped_groupid)
        self.cache_lock.release()
        return ret

    def manager_add_group_member(self,groupid,memberid,timestamp=0):
        stripped_memberid = memberid[0:15]
        self.cache_lock.acquire()
        self.cache.cache_add_group_member(groupid,stripped_memberid,timestamp)
        self.cache_lock.release()

    def manager_remove_group_member(self,groupid,memberid,timestamp=0):
        self.cache_lock.acquire()
        self.cache.cache_remove_group_member(groupid,memberid,timestamp)
        self.cache_lock.release()

    def manager_update_cache_member_info(self,list_of_member_details):
        if list_of_member_details == []:
            return 0
        else:
            self.cache_lock.acquire()
            for member in list_of_member_details:
                self.manager_add_member_details(member['id'].strip(),member['name']) 
            self.cache_lock.release()
             
    def manager_sync_group_info(self,group_dict):
        if (len(group_dict) == 0):
            return
        else:
            self.cache_lock.acquire()
            self.cache.cache_sync_group_info(group_dict)
            self.cache_lock.release()
        
    def manager_sync_member_and_group_info(self,member_info,grouplist):
        self.cache_lock.acquire()
        self.cache.cache_sync_member_and_group_info(member_info,grouplist)
        #cached_group_list = self.cache.cache_get_grouplist()
        cached_group_list = self.manager_get_self_membership_list()
        self.cache_lock.release()
        self.ui_lock.acquire()
        # Update the UI with latest list of groups
        for group in cached_group_list:
            if (self.ui_handle):
                self.ui_handle.addGroup(group)
        self.ui_lock.release()

    def manager_get_self_membership_list(self):
        self.cache_lock.acquire()
        glist = self.cache.cache_get_self_group_membership_list()
        self.cache_lock.release()
        return glist

    def manager_get_self_details(self):
        return self.cache.cache_get_self_details()

    ##################################
    # Group & member info functions :
    ##################################

    def manager_create_group(self,groupid):
        self_details = self.manager_get_self_details()
        memberid = self_details[0]
        unique_groupid = memberid+":"+str(groupid)
        if (self.manager_add_group(unique_groupid) == SUCCESS):
            self.manager_add_self_to_group(unique_groupid)
            return SUCCESS
        else:
            return ERROR

    def manager_join_group(self,groupid):
        return self.manager_add_self_to_group(groupid)

    def manager_leave_group(self,groupid):
        return self.manager_remove_self_from_group(groupid)

    def manager_frame_member_list_packet(self):
        sTemp = "M"
        sFinal = sTemp[0]
        self.cache_lock.acquire()
        member_list = self.cache.cache_get_all_member_details()
        self.cache_lock.release()
        member_count = len(member_list)
        sTemp = "%1s" % member_count
        sFinal += sTemp[0]
        for member in member_list:
            sTemp = "%15s" %member['id']
            sFinal += sTemp[0:15]
            sTemp = "%8s" %member['name']
            sFinal += sTemp[0:8]
        return sFinal

    def manager_deframe_member_list_packet(self,pkt):
        print ("Inside manager_deframe_member_list_packet(), packet received is ", pkt)
        if (pkt == ""):
            return []
        list_of_members = []
        counter = 0
        member_count = int(pkt[counter])
        counter += 1
        while (member_count > 0):
            d = dict()
            member_id = pkt[counter:counter+15]
            d['id'] = member_id
            counter += 15
            member_name = pkt[counter:counter+8]
            d['name'] = member_name
            list_of_members.append(d)
            counter += 8
            member_count -= 1
            del d
        return list_of_members

    def manager_frame_group_list_packet(self):
        self.cache_lock.acquire()
        list_of_groups = self.cache.cache_get_all_group_details()
        self.cache_lock.release()
        if (list_of_groups == []):
            return ""
        print (list_of_groups)
        sTemp = "G"
        sFinal = sTemp[0]
        group_count = len(list_of_groups)
        print ("Number of groups = ", group_count)
        sTemp = "%1s" % group_count
        sFinal += sTemp[0]
        for group in list_of_groups:
            gid = group['id']
            sTemp = "%19s" % gid
            sFinal += sTemp[0:19]
            timestamp = group['details'][0]
            print (timestamp)
            sTemp = "%3u" % timestamp
            sFinal += sTemp[0:3]
            member_count = len(group['details'][1])
            sTemp = "%2u" % member_count
            sFinal += sTemp[0:2]
            for member in group['details'][1]:
                sTemp = "%15s" % member
                sFinal += sTemp[0:15]
        print (sFinal)
        return sFinal
            
    def manager_deframe_group_list_packet(self,pkt):
        print ("Inside manager_deframe_group_list_packet(), packet received is ", pkt)
        if (pkt == ""):
            return []
        else:
            group_dict = dict()
            counter = 0
            group_count = int(pkt[counter])
            counter += 1
            while (group_count > 0):
                group_id = pkt[counter:counter+19].strip()
                counter += 19
                timestamp = int(pkt[counter:counter+3])
                counter += 3
                member_count = int(pkt[counter:counter+2])
                counter += 2
                list_of_members = []
                while (member_count > 0):
                    new_member = pkt[counter:counter+15].strip()
                    list_of_members.append(new_member)
                    counter += 15
                    member_count -= 1
                group_dict[group_id] = (timestamp,list_of_members)
                group_count -= 1 
        print ("Dictionary of group info : ", group_dict)
        return group_dict

    def manager_frame_improved_memberlist_packet(self):
        glist = self.manager_get_self_membership_list()
        memberinfo = self.manager_get_self_details()
        selfid = memberinfo[0]
        selfname = memberinfo[1]
        sTemp = "%15s" %selfid
        sFinal = ""
        sFinal += sTemp[0:15]
        sTemp = "%8s" %selfname
        sFinal += sTemp[0:8] 
        list_of_groups = self.manager_get_self_membership_list()
        group_count = len(list_of_groups)
        sTemp = "%1s" % group_count
        sFinal += sTemp[0]
        for group in list_of_groups:
            sTemp = "%19s" %group
            sFinal += sTemp[0:19]
        return sFinal

    def manager_deframe_improved_member_list_packet(self,pkt):
        if (pkt == ""):
            return []
        list_of_groups = []
        counter = 0
        memberid = pkt[counter:counter+15]
        counter += 15
        membername = pkt[counter:counter+8]
        counter += 8
        group_count = int(pkt[counter])
        counter += 1
        while (group_count > 0):
            groupid = pkt[counter:counter+19].strip()
            list_of_groups.append(groupid)
            counter += 19
            group_count -= 1
        member_details = (memberid.strip(),membername.strip())
        return (member_details,list_of_groups)

    #################
    # UI functions :
    #################

    def manager_display_text(self,groupid,message):
        self.ui_lock.acquire()         
        self.ui_handle.ui_streamtext(groupid,message)
        self.ui_lock.release()         

    def manager_start_ui(self):
        self.root = Tk()
        self.ui_handle = JabberAddaGUI(self.root,self)
        self.ui_handle.ui_addtext("Welcome to JabberAdda ")
        self.ui_handle.ui_addtext(SOFTWARE_VERSION)
        self.root.mainloop()

    def manager_show_fileopen_dialog(self):
        return self.ui_handle.WriteFile()
