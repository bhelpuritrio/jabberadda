##########################################################################

# ===========
# packetsr.py
# ===========

# This module implements a basic packet send/receive functionality
# for 2 types of messages sent over the network :
#   - Multicast control information (over a multicast address)
#   - Unicast UDP messages (for group jabber)

##########################################################################


import socket
import struct
import sys
import select
from enums import *
import os
from random import randint

class packetsr:
    def __init__(self, multicast_group, multicast_port, local_address, local_port = 800, local_tcp_server_port = 900):

        # Socket creation for Multicast Receive :
        #########################################

        self.recv_multicast_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # Bind to the server address
        self.multicast_server_address = (multicast_group, multicast_port)
        self.local_port = local_port
        self.recv_multicast_sock.bind(self.multicast_server_address)
        # Tell the operating system to add the socket to
        # the multicast group on all interfaces.
        group = socket.inet_aton(multicast_group)
        mreq = socket.inet_aton(multicast_group) + socket.inet_aton(local_address)
        self.recv_multicast_sock.setsockopt(
            socket.IPPROTO_IP,
            socket.IP_ADD_MEMBERSHIP,
            mreq)


        # Socket creation for Multicast Send :
        ######################################

        # Create the datagram socket
        self.send_multicast_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # Set the time-to-live for messages to 1 so they do not
        # go past the local network segment.
        ttl = struct.pack('b', 1)
        self.send_multicast_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        
        # Socket creation for UDP Unicast Send / Receive :
        ##################################################

        # Create the datagram socket
        self.unicast_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.unicast_sock.bind((local_address,local_port))

        # Socket creation for TCP Unicast Send & Receive (file transfer):
        ################################################################

        # Create socket for listening for file rx
        self.tcp_server_port = local_tcp_server_port
        self.tcp_server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server_sock.bind((local_address,local_tcp_server_port))
        self.tcp_server_sock.listen(TCP_MAX_CLIENT_CONN) # Start listening for incoming connections

    def tcp_wait_for_connect_requests(self):
        print("tcp_wait_for_connect_requests >> Before listening...")
        self.tcp_server_sock.listen(TCP_MAX_CLIENT_CONN)
        print("tcp_wait_for_connect_requests >> After listening...")
        return

    def tcp_receive_file_from_client(self):
        print("tcp_receive_file_from_client >> Before accept")
        c, addr = self.tcp_server_sock.accept()     # Establish connection with client.
        print("tcp_receive_file_from_client >> After accept")
        print("tcp_receive_file_from_client >> Connection requested from ", addr)
        sessionid = randint(0, 999999)
        dummyfile = "dummy" + str(sessionid)
        print("tcp_receive_file_from_client >> dummyfile is ", dummyfile)
        f = open(dummyfile,'wb')
        print ('tcp_receive_file_from_client >> Got connection from', addr)
        print ("tcp_receive_file_from_client >> Receiving...")
        l = c.recv(1024)
        while (l):
            f.write(l)
            l = c.recv(1024)
        f.close()
        print ("tcp_receive_file_from_client >> Done Receiving")
        c.shutdown(1)                # Shutdown the connection
        c.close()                    # Close the connection
        # Get the name of file & groupid from the header
        f = open(dummyfile,'rb')
        header = f.read(39);
        f.seek(39)
        header = header.decode('unicode_escape')
        print ("tcp_receive_file_from_client >> ", header,len(header))
        name = header[0:20].strip()         # Name of the file
        group_addr = header[20:39].strip()  # Groupid
        print ("tcp_receive_file_from_client >> Name of file : ",name)
        print ("tcp_receive_file_from_client >> Group address : ",group_addr)
        filename = str(randint(0,999))+name
        f2 = open("received_files/"+filename,'wb')
        f2.write(f.read())
        f.close()
        f2.close()
        os.remove(dummyfile) 
        return (addr,group_addr,filename) # Return client addr, groupid & name of file
        
    def tcp_insert_header_into_file(self,originalfile,string):
        # Create a temporary file with a random number affixed @ the end of filename
        sessionid = randint(0, 999999)
        sessionid = str(sessionid)
        tempfile = originalfile + sessionid
        print("Tempfile is ",tempfile)
        f2 = open(tempfile,'w')
        f = open(originalfile,'rb')
        f2.write(string)
        f2.close()
        f2 = open(tempfile,'ab')
        f2.write(f.read())
        f2.close()
        f.close()
        return tempfile

    def tcp_send_file(self,filename,destination,groupid):
        # Sanity check on input parameters
        if (groupid == "" or filename == "" or destination == ""):
            return ERROR
        # Open a TCP client sock & check if the destination is reachable / not
        self.tcp_client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ret = self.tcp_client_sock.connect_ex((destination,self.tcp_server_port))
        if (ret != 0):
            print ("tcp_send_file >> Client is busy, cannot send a file now!")
            return ERROR
        # Frame the header for the file, affix the groupid and filename 
        finalStr = ""
        name = os.path.split(filename)[1] # Get only the name of the file
        sTemp = "%20s" % name
        finalStr += sTemp
        group_addr = groupid
        sTemp = "%19s" % group_addr
        finalStr += sTemp
        print ("tcp_send_file >> framed header is ",finalStr,len(finalStr))
        tempfile = self.tcp_insert_header_into_file(filename,finalStr)
        print ("tcp_send_file >> tempfile is ", tempfile)
        f = open(tempfile,'rb')
        print ('Sending...')
        l = f.read(1024)
        while (l):
            self.tcp_client_sock.send(l)
            l = f.read(1024)
        f.close()
        print ("tcp_send_file() : Done Sending")
        #s.shutdown(socket.SHUT_WR)
        self.tcp_client_sock.shutdown(1)
        self.tcp_client_sock.close()
        #del self.tcp_client_sock
        os.remove(tempfile)
        return SUCCESS

    def send_packet(self,multicast_flag, packet_as_string, local_address=''):
        #if multicast flag is set, send the specified packet to the multicast socket
        if(multicast_flag == 1):
            self.send_multicast_sock.sendto(bytes(packet_as_string, 'UTF-8'), self.multicast_server_address)
        else:
            self.unicast_sock.sendto(bytes(packet_as_string, 'UTF-8'), (local_address,self.local_port))
            
    def recv_multicast_packet(self):
        (data, address) = self.recv_multicast_sock.recvfrom(1024)
        return data,address

    def wait_for_packet_events(self):
        listening_list = [self.tcp_server_sock,self.unicast_sock,self.recv_multicast_sock]   
        readable,writable,exceptional = select.select(listening_list,[],listening_list) 
        event_list = []
        for s in readable:
            if s is self.recv_multicast_sock:
                event_list.append('mcast')
            elif s is self.unicast_sock:
                event_list.append('ucast')
            elif s is self.tcp_server_sock:
                event_list.append('tcp')
        return event_list
            
    def recv_unicast_packet(self):
        (data, address) = self.unicast_sock.recvfrom(1024)
        return data,address
    
