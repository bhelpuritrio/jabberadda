import packetsr

new = packetsr.packetsr('224.3.29.71', 10000, '10.0.0.6', 800)
print ('Waiting for a file from client')
new.tcp_wait_for_connect_requests()
ret = new.tcp_receive_file_from_client()
print ("Received file details : ", ret)
print ("Return value : ", ret)
print ("Sender was ", ret[0][0])
print ("Groupid was ", ret[1])
print ("File sent was ", ret[2])
