import packetsr

new = packetsr.packetsr('224.3.29.71', 10000, '10.0.0.5', 800)
print ("Waiting for MCast / UCast packet reception")
while 1:
	events = new.wait_for_packet_events()
	print(events)
	if 'mcast' in events:
		print("MCast data received!")
		data,address = new.recv_multicast_packet()
		print ("Data received was ", data, " from ", address)
	if 'ucast' in events:
		print("UCast data received!")
		data,address = new.recv_unicast_packet()
		print ("Data received was ", data, " from ", address)
