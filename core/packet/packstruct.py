import datetime

from enum import Enum

class MessageType(Enum):
    send_msg=1
    send_file=2
    membership_info=3


class PacketHeader:
    #private values
    eMessageType = MessageType(1)
    iGroupID =int()
    #iIP = int()
    tTimeStamp = datetime.datetime.now()
    bReqRes = bool()
    #sName = str()
    iSeqID = int()
    iAckID = int()
    iMsgID =int()
    iPktSize =int()
    #Consturctors also auto parser
    def __init__(self)->None:
        pass
    def FillFromString(self, value )->None:
        iLoop=0
        self.eMessageType = MessageType(int(value[iLoop]))
        iLoop+=1
        self.iGroupID = value[iLoop:iLoop+19]
        iLoop+=19
# This field is redundant (already sent by the IP layer)
#        self.iIP = int(value[iLoop:iLoop+12])
#        iLoop+=12
        if True:
            sTempStr = value[iLoop:iLoop+20]
            self.tTimeStamp = datetime.datetime(int(sTempStr[0:4]),int(sTempStr[4:6]),int(sTempStr[6:8]),int(sTempStr[8:10]),int(sTempStr[10:12]),int(sTempStr[12:14]),int(sTempStr[14:20]))
        iLoop+=20
        self.bReqRes=bool(int(value[iLoop]))
        iLoop+=1
# This field is redundant (already sent by the membership updation thread)
#        self.sName=value[iLoop:iLoop+8]
#        iLoop+=8
        self.iSeqID = int(value[iLoop:iLoop+5])
        iLoop+=5
        self.iAckID = int(value[iLoop:iLoop+5])
        iLoop+=5
        self.iMsgID = int(value[iLoop:iLoop+5])
        iLoop+=5
        self.iPktSize = int(value[iLoop:iLoop+5])
    #Getters and Setter, will add asserts later
    def SetMessageType(self, value) -> None:
        if isinstance(value,MessageType):
            self.eMessageType = value
        else:
            self.eMessageType = MessageType(int(value))
    def GetMessageType(self):
        return self.eMessageType
    def SetGroupID(self, value) -> None:
        self.iGroupID = value
    #def SetName(self, value) -> None:
     #   self.sName = value
    #def GetName(self):
     #   return self.sName
    def GetGroupID(self):
        return self.iGroupID
#    def SetIP(self, value) -> None:
 #       self.iIP = int(value)
 #   def GetIP(self):
  #      return self.iIP
    def SetTimeStamp(self,value)->None:
        if isinstance(value,datetime.datetime):
            self.tTimeStamp = value
        else:
            self.tTimeStamp = datetime.datetime(value)
    def GetTimeStamp(self):
        return self.tTimeStamp
    def SetReqRes(self, value)->None:
        self.bReqRes =bool(value)
    def GetReqRes(self):
        return self.bReqRes
    def SetSeqID(self,value)->None:
        self.iSeqID =int(value)
    def GetSeqID(self):
        return self.iSeqID
    def SetAckID(self,value)->None:
        self.iAckID =int(value)
    def GetAckID(self):
        return self.iAckID
    def SetMsgID(self,value)->None:
        self.iMsgID =int(value)
    def GetMsgID(self):
        return self.iMsgID
    def SetPktSize(self,value)->None:
        self.iPktSize =int(value)
    def GetPktSize(self):
        return self.iPktSize

    def FormStringHeader(self):
        sTemp = "%1s" % self.eMessageType.value
        sFinal = sTemp[0]
        sTemp = "%19s" % self.iGroupID
        sFinal = sFinal + sTemp[0:19]
# This field is redundant (already sent by the IP layer)
#        sTemp = "%12u" %self.iIP
#        sFinal = sFinal + sTemp[0:12]
        sTemp = "%4u%2u%2u%2u%2u%2u%6u" % (self.tTimeStamp.year,self.tTimeStamp.month,self.tTimeStamp.day,self.tTimeStamp.hour,self.tTimeStamp.minute,self.tTimeStamp.second,self.tTimeStamp.microsecond)
        sFinal = sFinal + sTemp[0:20]
        sTemp = "%1u" % self.bReqRes
        sFinal = sFinal + sTemp[0]
# This field is redundant (already sent by membership updation thread)
#        sTemp = "%8s" % self.sName
#        sFinal = sFinal + sTemp[0:8]
        sTemp = "%5u" % self.iSeqID
        sFinal = sFinal + sTemp[0:5]
        sTemp = "%5u" % self.iAckID
        sFinal = sFinal + sTemp[0:5]
        sTemp = "%5u" % self.iMsgID
        sFinal = sFinal + sTemp[0:5]
        sTemp = "%5u" % self.iPktSize
        sFinal = sFinal + sTemp[0:5]
        return sFinal

    def StripAndFill(self,text):
        self.FillFromString(text)
        obj = self.FormStringHeader()
        return text[len(obj):len(text)]