# This file houses enums / constants that shall be
# used in the project

# Common status / error codes

SOFTWARE_VERSION="V0.1 (alpha)"

SUCCESS = 0
ERROR = 1
TCP_MAX_CLIENT_CONN = 10
