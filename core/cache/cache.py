##########################################################################

# ========
# cache.py
# ========

# This module implements a cache to store a list of groups
# with their associated set of group members for retrieval
# later by the groupchat program.

# The cache is implemented with 2 dictionaries :
# 1st dictionary : Dictionary of <groupid,groupinfo> pair
#                                          |
#                                          |-> Which has a linked list of member IDs

# 2nd dictionary : Dictionary of <memberid,memberinfo> pair

##########################################################################

from enums import *

# Class 'memberinfo' :
######################

class memberinfo:

    # "Private" members :
    #####################

    memberid = 0
    name = ""

    # Constructor(s) :
    ##################

    def __init__(self, id):
        self.memberid = id

    def __init__(self, id, name):
        self.memberid = id
        self.name = name

    # Functions :
    #############

    def memberinfo_getid(self):
        return self.memberid

    def memberinfo_getname(self):
        return self.name

    def memberinfo_getdetails(self):
        d = dict()
        d['id'] = self.memberid
        d['name'] = self.name
        return d

##########################################################################

# Class 'groupinfo' :
#####################

class groupinfo:

    # "Private" members :
    #####################

    groupid = 0;
    memberlist = []

    # Constructor :
    ###############

    def __init__(self, id,timestamp):
        self.groupid = id
        self.groupsize = 0
        self.timestamp = timestamp
        self.memberlist = list()

    # Functions :
    #############

    def groupinfo_getgroupid(self):
        return self.groupid

    def groupinfo_searchmember(self, memberid):
        return memberid in self.memberlist

    def groupinfo_getmemberids(self,groupid):
        self.memberlist.sort()
        return self.memberlist

    def groupinfo_addmember(self, memberid):
        if (not self.groupinfo_searchmember(memberid)):
            self.memberlist.append(memberid)
            self.groupsize+=1
            return SUCCESS
        else:
            return ERROR

    def groupinfo_remove_members(self):
        del self.memberlist
        self.groupsize = 0

    def groupinfo_remove_member(self,memberid):
        if (self.groupinfo_searchmember(memberid)):
            self.memberlist.remove(memberid)
            self.groupsize-=1
            return SUCCESS
        else:
            return ERROR

    def groupinfo_updatetimestamp(self,timestamp):
        if (timestamp != 0):
            self.timestamp = timestamp

    def groupinfo_gettimestamp(self):
        return self.timestamp

    def groupinfo_getgroupsize(self):
        return self.groupsize

##########################################################################

# Class 'cache' :
#################

class cache:

    # "Private" members :
    #####################

    group_dict = dict()
    member_dict = dict()

    # Constructor :
    ###############

    def __init__(self,ip_addr,name):
        # The following 2 dictionaries represent the global
        # topology of group & member info
        self.group_dict = dict()
        self.member_dict = dict()
        # The following 2 members are the host's IP address
        # & hostname
        self.memberid = ip_addr
        self.name = name
        self.cache_add_member(ip_addr,name)
        # This list represents the list of groups for which the
        # the host is a member of, useful for detecting inconsistencies
        # in the global list shared across hosts ( we don't wish to be excluded
        # from groups which we're actually part of / be included in groups that
        # we're not part of )
        self.personal_group_list = []

    # Functions :
    #############

    # Membership dictionary functions :
    ###################################

    def cache_searchmember(self, memberid):
        return memberid in self.member_dict

    def cache_get_memberlist(self):
        return self.member_dict.keys()

    def cache_get_memberlistwithdetails(self,groupid):
        keys = self.member_dict.keys()
        list_of_members = []
        for index in keys:
            list_of_members.append(self.member_dict[index].memberinfo_getdetails())
        return list_of_members

    def cache_add_member(self,memberid,name):

        if (not self.cache_searchmember(memberid)):
            self.member_dict[memberid] = memberinfo(memberid,name)
            return SUCCESS
        else:
            return ERROR

    def cache_get_memberdetails(self,memberid):
        if (self.cache_searchmember(memberid)):
            return self.member_dict[memberid].memberinfo_getdetails()

    def cache_remove_member(self,memberid):
        if memberid not in self.member_dict.keys():
            return ERROR
        else:
            for groupid in self.group_dict.keys():
                self.cache_remove_group_member(groupid,memberid)
            self.member_dict.pop(memberid)
            return SUCCESS

    # Group operations :
    ####################

    def cache_add_group(self,groupid,timestamp=0):
        if (self.cache_search_group(groupid)):
            return ERROR
        else:
            newgroup = groupinfo(groupid,timestamp)
            self.group_dict[groupid] = newgroup
            return SUCCESS

    def cache_search_group(self,groupid):
        return groupid in self.group_dict

    def cache_remove_group(self,groupid):
        if (self.cache_search_group(groupid)):
            self.group_dict[groupid].groupinfo_remove_members()
            self.group_dict.pop(groupid)
            return SUCCESS
        else:
            return ERROR

    def cache_get_grouplist(self):
        list_of_groups = self.group_dict.keys()
        #list_of_groups.sort() - Not available on Py 3.5
        return list_of_groups

    # Group <-> member operations :
    ###############################

    def cache_search_member_in_group(self,groupid,memberid):
        return self.group_dict[groupid].groupinfo_searchmember(memberid)

    def cache_add_group_member(self,groupid,memberid,timestamp=0):
        if (self.cache_search_group(groupid)):
            if (self.cache_searchmember(memberid)):
                if (SUCCESS == self.group_dict[groupid].groupinfo_addmember(memberid)):
                    self.group_dict[groupid].groupinfo_updatetimestamp(timestamp)
                    return SUCCESS
                else:
                    return ERROR
        return ERROR

    def cache_get_self_details(self):
        return (self.memberid,self.name)

    def cache_get_self_group_membership_list(self):
        return self.personal_group_list

    def cache_add_self_to_group(self,groupid):
        if (self.cache_search_group(groupid)):
            if groupid not in self.personal_group_list:
                self.personal_group_list.append(groupid)
                self.cache_add_group_member(groupid,self.memberid)
                return SUCCESS
        else:
            return ERROR

    def cache_remove_group_member(self,groupid,memberid,timestamp=0):
        if (self.cache_search_group(groupid)):
            if (SUCCESS == self.group_dict[groupid].groupinfo_remove_member(memberid)):
                self.group_dict[groupid].groupinfo_updatetimestamp(timestamp)
                return SUCCESS
            else:
                return ERROR
        else:
            return ERROR

    def cache_remove_self_from_group(self,groupid):
        if (self.cache_search_group(groupid)):
            if groupid in self.personal_group_list:
                self.personal_group_list.remove(groupid)
                self.cache_remove_group_member(groupid,self.memberid)
                return SUCCESS
        else:
            return ERROR
        

    def cache_remove_group_member_and_group_if_empty(self,groupid,memberid,timestamp=0):
        if (self.cache_search_group(groupid)):
            if (SUCCESS == self.group_dict[groupid].groupinfo_remove_member(memberid)):
                self.group_dict[groupid].groupinfo_updatetimestamp(timestamp)
            if (self.group_dict[groupid].groupinfo_getmemberids(groupid) == []):
                self.cache_remove_group(groupid)
            return SUCCESS
        else:
            return ERROR

    def cache_create_member_and_add_to_group(self,groupid,memberid,membername=None,timestamp=0):
        if (self.cache_search_group(groupid)):
            if (membername is not None):
                self.cache_add_member(memberid,membername)
            self.cache_add_group_member(groupid,memberid,timestamp)
            return SUCCESS
        else:
            return ERROR

    def cache_create_group_and_member(self,groupid,memberid,membername=None,timestamp=0):
        if (not self.cache_search_group(groupid)):
            self.cache_add_group(groupid,timestamp)
        return self.cache_create_member_and_add_to_group(groupid,memberid,membername,timestamp)

    def cache_get_group_members(self,groupid):
        if (self.cache_search_group(groupid)):
            return self.group_dict[groupid].groupinfo_getmemberids(groupid);
        else:
            return []

    def cache_get_group_member_details(self,groupid):
        list_of_members = []
        list_of_member_details = []
        if (self.cache_search_group(groupid)):
            list_of_members = self.cache_get_group_members(groupid)
            for index in list_of_members:
                list_of_member_details.append(self.cache_get_memberdetails(index))
            return list_of_member_details
        else:
            return []

    def cache_get_all_group_details(self):
        list_of_group_details = []
        for group in self.group_dict.keys():
            d = dict()
            d['id']=group
            d['details']=(self.group_dict[group].groupinfo_gettimestamp(),self.cache_get_group_members(group))
            list_of_group_details.append(d)
            del d
        return list_of_group_details

    def cache_print_all_group_details(self):
        print ("List of groups :")
        print ("################")
        for group in self.group_dict.keys():
            print ("Groupid is : ", group, "; size is : ", self.group_dict[group].groupinfo_getgroupsize(), "; timestamp is:", self.group_dict[group].groupinfo_gettimestamp())
            print ("  Members are :", self.cache_get_group_members(group))
            print ("\n")

    def cache_get_all_member_details(self):
        list_of_member_details=[]
        for member in self.member_dict.keys():
            temp = self.cache_get_memberdetails(member)
            list_of_member_details.append(temp)
        return list_of_member_details

    def cache_print_all_member_details(self):
        print ("Member details are :")
        print ("####################")
        member_list = self.cache_get_all_member_details()
        for member in member_list:
            print ("Member ID : ",member['id'],"; Name : ",member['name'])

    def cache_sync_member_and_group_info(self,memberinfo,grouplist):
        memberid = memberinfo[0]
        membername = memberinfo[1]
        # Add the member info received from N/W
        self.cache_add_member(memberid,membername)
        # Update cache with new groups (if any) & add this member to the same
        for group in grouplist:
            self.cache_add_group(group)
            self.cache_add_group_member(group,memberid)
        # Remove this member from a group (cached info) if he doesn't
        # currently belong to the same
        cached_group_list = self.cache_get_grouplist()
        for group in cached_group_list:
            if (self.cache_search_member_in_group(group,memberid)):
                if group not in grouplist:
                    self.cache_remove_group_member(group,memberid)
         
    
######################################
#### The below API are deprecated ####
######################################

    def cache_check_if_all_members_exist(self,member_list):
        # If there are no members in the list, we'll return SUCCESS since there's
        # a chance that the concerned group is now empty with no participants
        if (member_list == []):
            return SUCCESS
        else:
        # Otherwise, check the set membership of received grouplist
            cached_member_list = self.cache_get_memberlist()
            for member in member_list:
                if member in cached_member_list:
                    pass
                else:
                    return ERROR
            return SUCCESS

    def cache_update_group_member_list(self,groupid,member_list,timestamp):
        self.cache_remove_group(groupid)
        self.cache_add_group(groupid,timestamp)
        if (member_list == []):
            # If member list if empty, don't do anything ! 
            # The group could be empty...
            pass
        else:
            for member in member_list:
                self.cache_add_group_member(groupid,member,timestamp)  

    def cache_sync_group_info(self,group_dict):
        if (len(group_dict) == 0):
            return
        else:
            for group in group_dict.keys():
                # Check if the group exists, & if yes...
                if (self.cache_search_group(group)):
                    # If the received group info's timestamp is greater than cached timestamp...
                    if (group_dict[group][0] > self.group_dict[group].groupinfo_gettimestamp()):
                        # If all members of the received group info exist in cache...
                        if (SUCCESS == self.cache_check_if_all_members_exist(group_dict[group][1])):
                            # Update the cache with latest member list of the respective group
                            self.cache_update_group_member_list(group,group_dict[group][1],group_dict[group][0])
                # If the group doesn't exist...
                else:
                    # If all members of the received group info exist in cache...
                    if (SUCCESS == self.cache_check_if_all_members_exist(group_dict[group][1])):
                        # Add the new group & its members
                        self.cache_update_group_member_list(group,group_dict[group][1],group_dict[group][0]) 
                        
##########################################################################
