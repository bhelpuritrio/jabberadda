from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog
import tkinter.font as tkFont
from PIL import ImageTk, Image
import os
from enums import *
import manager
from tkinter.scrolledtext import ScrolledText

class JabberAddaGUI:
    MsgMem = dict()
    
    def __init__(self, master, manager_instance):
        self.selected_groupid = 0
        self.master = master
        self.manager_instance = manager_instance
        self.self_ip_and_name = self.manager_instance.manager_get_self_details()
        self.self_ip = self.self_ip_and_name[0]
        self.self_name = self.self_ip_and_name[1]
        master.title("JabberAdda ( "+self.self_name+", "+self.self_ip+")")

        self.LabelFont = tkFont.Font(family="Helvetica", size=12)
        self.TextBoxFont = tkFont.Font(family="Helvetica", size=10)

        self.labelGroup = Label(master,text="Group:",font=self.LabelFont)
        self.labelMessages = Label(master,text="Message Window:",font=self.LabelFont)
        #self.textbox = Text(master,height=10,width=60,font=self.TextBoxFont)
        self.textbox = ScrolledText(master,height=5,width=60,font=self.TextBoxFont)
        #self.scrolling = Scrollbar(master=self.master,orient=VERTICAL,command = self.textbox.yview)
        #self.textbox.configure(yscrollcommand=self.scrolling.set)
        cwd = os.getcwd()
        self.logo = ImageTk.PhotoImage(Image.open(cwd+"/core/ui/logo2.png"))
        selectedText = StringVar()
        userText = StringVar()
        selectedText.set("Something here")
        self.selection = Combobox(self.master, width=20, textvariable=selectedText,state = "readonly")
        self.selection['values'] = ()
        self.selection.grid(column=0, row=1)
        self.selection.current()
        self.panel = Label(master, image = self.logo)
        self.goButton = Button(self.master, text = "Go to Group", command = self.goFunction)
        self.sendmsgButton = Button(self.master, text = "Send message", command = self.sendMessage)
        self.userinputlabel = Label(master,text="User Input:",font=self.LabelFont)
        self.usermessage = ScrolledText(self.master,width=60,height=5)
        #self.greet_button = Button(master, text="Greet", command=self.greet)
        #self.close_button = Button(master, text="Close", command=master.quit)
        self.fileSelectButton = Button(self.master, text = "Select File",command = self.GetFile)
        #self.selectedFile = StringVar()
        #self.fileSelectLabel = Label(master, text = "Select File",font=self.LabelFont,textvariable=self.selectedFile)
        self.fileSelectLabel = Label(master, text = "Select File",font=self.LabelFont)
        self.fileSendButton = Button(self.master, text = "Send File", command = self.SendFile)

        self.selection.grid(row=1,column = 0)
        self.selection.menu = Menu(self.selection, tearoff=0)
        self.panel.grid(row=0)
        self.labelGroup.grid(row=1, columnspan=2, sticky=W)
        self.goButton.grid(row=1,column=1,sticky=W)
        self.labelMessages.grid(row=2, columnspan=2, sticky=W)
        self.textbox.grid(row=3,sticky ="ns")
        #self.scrolling.grid(row=3, column =1, sticky = 'ns')
        #self.greet_button.grid(row=1, column=0)
        #self.close_button.grid(row=1, column=1)
        self.userinputlabel.grid(row=4,columnspan=2,sticky=W)
        #self.usermessage.grid(row=5,rowspan=20,columnspan=2,sticky="ns")
        self.usermessage.grid(row=5,sticky="ns")
        self.sendmsgButton.grid(row=5,column=1)
        self.fileSelectButton.grid(row=7,column=0,sticky=W)
        self.fileSelectLabel.grid(row=7,column=0,sticky=E)
        self.fileSendButton.grid(row=7,column=1,sticky=W)

    def greet(self):
        print("Sorry! No groups selected.")

    def ui_addtext(self,text):
        self.textbox.insert(END,text)
        self.textbox.see(END)

    def addGroup(self,text)->None:
        temp = self.selection['values']

        if text in temp:
            pass
        else:
            temp+=(text,)
            self.selection['values'] =temp
            self.MsgMem[text] = ""


    def delGroup(self,text)->None:

        temp = self.selection['values']
        temp2 = tuple()
        for x in temp:
            if x!= text:
                temp2+=(x,)
            else:
                del self.MsgMem[x]
        self.selection['values'] = temp2

    def cleanSlate(self)->None:
        self.textbox.delete("1.0",END)

    def sendMessage(self)->None:
        text = self.usermessage.get("1.0",END)
        self.usermessage.delete("1.0",END)
        if (self.manager_instance.manager_search_group(self.selected_groupid)):
            self.manager_instance.manager_send_message(text,self.selected_groupid)

    def goFunction(self)->None:
        self.cleanSlate()
        idtex = self.selection.get()

        if idtex in self.selection['values']:
            temp = self.MsgMem[idtex]
            self.ui_addtext(temp)
            self.selected_groupid = idtex
        else:
            pass
            #self.ui_addtext("Oops!\n")

    def ui_streamtext(self,idtext, text):
        if idtext in self.selection['values']:
            #self.MsgMem[idtext] = self.MsgMem[idtext]+"\n"+text
            self.MsgMem[idtext] = self.MsgMem[idtext]+text
            self.goFunction()

    def ui_set_manager_instance(self,manager_instance):
        self.manager_instance = manager_instance

    def WriteFile(self):
        temp_dir = filedialog.asksaveasfilename()
        return temp_dir

    def GetFile(self):
        #self.selectedFile.set(filedialog.askopenfilename())
        self.fileSelectLabel['text'] = filedialog.askopenfilename()

    def SendFile(self):
        filename = self.fileSelectLabel['text']
        if (self.manager_instance.manager_search_group(self.selected_groupid)):
            if (os.path.isfile(filename)):
                self.ui_addtext ("Great! Sending the file " + filename + " to group " + self.selected_groupid+"\n")
                self.manager_instance.manager_send_file_async(filename,self.selected_groupid)
            else:
                self.ui_addtext("Sorry! The file " + filename + " does not exist !")
        else:
            self.ui_addtext("\nACHTUNG! Kindly select a valid group before sending a file.")
            
