from tkinter import *
from ui import *

root = Tk()
my_gui = JabberAddaGUI(root)
my_gui.ui_addtext("Namaste, Welcome to JabberAdda ")
my_gui.ui_addtext(SOFTWARE_VERSION)
my_gui.addGroup("10.0.0.1:47")
my_gui.addGroup("10.0.1.2:123")

my_gui.ui_streamtext("10.0.0.1:47","4This is how text is displayed")
my_gui.ui_streamtext("10.0.1.2:123","3This is how text is displayed")
my_gui.ui_streamtext("10.0.0.1:47","This is how text is displayed")
my_gui.ui_streamtext("10.0.1.2:123","1This is how text is displayed")
my_gui.ui_streamtext("10.0.1.2:123","2This is how text is displayed")
my_gui.ui_streamtext("10.0.0.1:47","5This is how text is displayed")
my_gui.ui_streamtext("10.0.1.2:123","6This is how text is displayed")
my_gui.ui_streamtext("10.0.1.2:123","7This is how text is displayed")
my_gui.ui_streamtext("10.0.0.1:47","8This is how text is displayed")
#my_gui.delGroup("hello")
print(my_gui.GetFile())
print(my_gui.WriteFile())
#my_gui.cleanSlate()
root.mainloop()
