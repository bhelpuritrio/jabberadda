import sys
import time
import manager
import cmd
from enums import *
import os

class CmdParser(cmd.Cmd):
    prompt = 'JabberAdda >> '
    intro = '\n<==== Welcome to JabberAdda : A simple group chat program ====>\n'

    def set_manager(self,manager_instance):
        self.manager_instance = manager_instance

    def do_start_group(self, groupid):
        "Start a group (range of numbers 0->255)"
        if (not groupid.isdigit()):
            print ("Sorry! Please enter a valid groupid")
        else:
            groupid_int = int(groupid)
            if (not 0<=groupid_int<=255):
                print ("Sorry! Please enter a groupid in the range [0:255]")
            else:
                if (SUCCESS == self.manager_instance.manager_create_group(groupid)):
                    print ("\nGreat, starting a group with groupid : ",groupid, "\n") 
                else:
                    print ("\nSorry, that group already exists!\n")

    def do_join_group(self, groupid):
        "Join a group"
        if (SUCCESS == self.manager_instance.manager_join_group(groupid)):
            print ("\nGreat, joining a group with groupid : ",groupid, "\n") 
        else:
            print ("\nSorry, you've already joined that group / that group doesn't exist!\n")

    def do_leave_group(self, groupid):
        "Leave a group"
        if (SUCCESS == self.manager_instance.manager_leave_group(groupid)):
            print ("\nGreat, left the group with groupid : ",groupid, "\n") 
        else:
            print ("\nSorry, you've already left that group / that group doesn't exist anymore!\n")

    def do_query_groups(self, args):
        "Display the list of groups created along with their respective members"
        print("")
        self.manager_instance.manager_print_all_group_details() 
    
    def do_query_members(self,args):
        "Displays the list of members currently present on JabberAdda"
        print("")
        self.manager_instance.manager_print_all_member_details()
        print("")

    def do_send_msg(self, args):
        "Send a message to a group"
        list = args.split()
        groupid = list[0]
        message = ' '.join(list[1:])
        message = message + "\n"
        if (not self.manager_instance.manager_search_group(groupid)):
            print ("Sorry! This groupid ", groupid," doesn't exist!")
        if (message == "" or message == " "):
            print ("Sorry! Please enter a valid non-empty message")
        else:
            self.manager_instance.manager_send_message(message,groupid)
            print("Message sent!")
    
    def do_send_file(self,args):
        "Send a file to a group"
        list = args.split()
        if (len(list) < 2):
            print ("Sorry! Atleast 2 arguments <groupid> <filename> are required")
        else:
            groupid = list[0]
            filename = list[1]
            if (self.manager_instance.manager_search_group(groupid)):
                if (os.path.isfile(filename)):
                    print ("Great! Sending the file ", filename, " to group ",groupid)
                    self.manager_instance.manager_send_file_async(filename,groupid)
                else:
                    print("Sorry! The file ",filename, " does not exist !")
            else:
                print ("Sorry! This groupid ", groupid," doesn't exist!")
    
    def complete_greet(self, text, line, begidx, endidx):
        return "<groupid (0-255)>"
    
    def do_EOF(self, line):
        print("Bye bye, see you soon !")
        self.manager_instance.manager_app_quit()
        return True


def test_code(manager_instance):
    print ("Inside test code")
    manager_instance.manager_add_member_details("10.0.0.9","Vivekananda")
    manager_instance.manager_add_member_details("10.0.0.11","Muruganar")
    #manager_instance.manager_print_all_member_details()
    #time.sleep(5)
    manager_instance.manager_add_member_details("10.0.0.7","Annamalai Swami")
    manager_instance.manager_add_member_details("10.0.0.8","Sri Sadhu Om")
    manager_instance.manager_add_group("10.0.0.9:242")
    manager_instance.manager_add_group("10.0.0.11:222")
    manager_instance.manager_add_group("10.0.0.10:223")
    manager_instance.manager_add_group_member("10.0.0.9:242", "10.0.0.7", 1)
    manager_instance.manager_add_group_member("10.0.0.9:242", "10.0.0.9", 2)
    manager_instance.manager_add_group_member("10.0.0.11:222", "10.0.0.6", 7)
    manager_instance.manager_add_group_member("10.0.0.11:222", "10.0.0.8", 7)
    manager_instance.manager_add_group_member("10.0.0.10:223", "10.0.0.9", 20)
    manager_instance.manager_add_group_member("10.0.0.10:223", "10.0.0.6", 33)
    manager_instance.manager_print_all_member_details()
    manager_instance.manager_print_all_group_details()
    manager_instance.manager_create_group("192")
    manager_instance.manager_create_group("294")
    manager_instance.manager_create_group("338")
    manager_instance.manager_print_all_group_details()
    manager_instance.manager_join_group("10.0.0.9:242")
    manager_instance.manager_join_group("10.0.0.11:222")
    manager_instance.manager_print_all_group_details()
    time.sleep(5)
    manager_instance.manager_join_group("10.0.0.6:222")
    manager_instance.manager_join_group("10.0.0.6:223")
    manager_instance.manager_join_group("10.0.0.6:242")
    manager_instance.manager_print_all_group_details()

    
