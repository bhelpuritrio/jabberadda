import sys
import random
import time
import manager
from packstruct import MessageType, PacketHeader
import datetime

# Unicast packet processing interval
ucast_process_interval=5
mcast_process_interval=1

def packet_receive_thread(args,manager_instance):
    print ("Started packet_receive_thread()")
    while 1:
        if (manager_instance.manager_check_quit()):
            break
        events = manager_instance.manager_wait_for_packet_receive_events()
        if 'mcast' in events:
            data,address = manager_instance.manager_recv_multicast_packet()
            manager_instance.manager_queue_mcast_packet((data,address))
        if 'ucast' in events:
            data,address = manager_instance.manager_recv_unicast_packet()
            manager_instance.manager_queue_ucast_packet((data,address))
        if 'tcp' in events:
            ret = manager_instance.manager_tcp_receive_file_async()
                
    print ("Exiting packet_receive_thread...")
    return 0

def ucast_processing_thread(args,manager_instance):
    print ("Started ucast_processing_thread()")
    obj = PacketHeader()
    pktheader = obj.FormStringHeader()
    header_size = len(pktheader)	
    while 1:
        if (manager_instance.manager_check_quit()):
            break
        #time.sleep(ucast_process_interval)
        data = manager_instance.manager_dequeue_ucast_packet()
        if (data == []):
            continue
        else:
            packet = data[0].decode("utf-8")
            obj.FillFromString(packet)
            request_type = obj.GetMessageType()
            reqres = obj.GetReqRes()
            groupid = obj.GetGroupID()
            ackid = obj.GetAckID()
            src_ip_addr = data[1][0]
            groupid = groupid.strip()
            if (request_type == MessageType.send_msg):
                # If this is a Unicast message sent by someone which needs to be displayed...
                if (reqres == 0):
                    manager_instance.manager_handle_message_display(packet[header_size:],groupid,data[1]) 
                    response = ""
                    obj.SetAckID(obj.GetSeqID()+1)
                    obj.SetReqRes(1)
                    obj.SetPktSize(0)
                    msgid = random.randint(1,9999)
                    obj.SetMsgID(msgid)
                    obj.SetSeqID(msgid)
                    obj.SetTimeStamp(datetime.datetime.now())
                    response_header = obj.FormStringHeader()
                    manager_instance.manager_send_unicast_packet(response_header,src_ip_addr)
                # If this is an ACK for a unicast message which was sent earlier by us...
                else:
                    print("ACK# ",ackid," received from ", data[1], "; will send to manager")
                    manager_instance.manager_store_ack(src_ip_addr,ackid,packet)
    print ("Exiting ucast_processing_thread...")
    return 0

def mcast_processing_thread(args,manager_instance):
    print ("Started mcast_processing_thread()")
    obj = PacketHeader()
    pktheader = obj.FormStringHeader()
    header_size = len(pktheader)	
    while 1:
        if (manager_instance.manager_check_quit()):
            break
        #time.sleep(mcast_process_interval)
        data = manager_instance.manager_dequeue_mcast_packet()
        if (data == []):
            continue
        else:
            packet = data[0].decode("utf-8")
            obj.FillFromString(packet)
            request_type = obj.GetMessageType()
            if (request_type == MessageType.membership_info):
                pkt = packet[header_size:]
                member_and_group_details = manager_instance.manager_deframe_improved_member_list_packet(pkt)
                member_info = member_and_group_details[0]
                grouplist = member_and_group_details[1]
                manager_instance.manager_sync_member_and_group_info(member_info,grouplist)
    print ("Exiting mcast_processing_thread...")
    return 0
