import sys
import time
import manager

membership_broadcast_sleep_interval=0.5

def membership_broadcast_thread(self,manager_instance):
    print ("Started membership_broadcast_thread()")
    while 1:
        if (manager_instance.manager_check_quit()):
            break
        time.sleep(membership_broadcast_sleep_interval)
        pkt = manager_instance.manager_frame_improved_memberlist_packet()
        manager_instance.manager_multicast_membership_info(pkt)
    print("Exiting membership_broadcast_thread...")
    return 0
